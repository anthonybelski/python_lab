r"""
The module for outputting tasks
"""

from taskapp.library.logger import get_logger, logger_settings

DEFAULT_SPACES_FIELD = 0
GROUP_SPACES_FIELD = 4

IN_PROCESS_FIELD = 'In the process'
COMPLETED_FIELD = 'Completed'


def show_tasks(storage, *args):
    tasks = storage.filter_tasks(IN_PROCESS_FIELD, True)
    for task in tasks:
        output(task, DEFAULT_SPACES_FIELD, *args)
    logger_settings(get_logger()).info('The tasks with information about them were output.')


def show_with_subtasks(storage, *args):
    tasks = storage.filter_tasks(IN_PROCESS_FIELD)
    for task in tasks:
        if not task.parent_id:
            output(task, DEFAULT_SPACES_FIELD, *args)
            if task.subtasks:
                show_subtasks(task, *args)
    logger_settings(get_logger()).info('The tasks with their subtasks were output.')


def show_subtasks(task, *args, spaces=4):
    for subtask in task.subtasks:
        output(subtask, spaces, *args)
        if subtask.subtasks:
            show_subtasks(subtask, *args, spaces + 4)


def show_completed(storage, *args):
    tasks = storage.filter_tasks(COMPLETED_FIELD)
    for task in tasks:
        if not task.parent_id:
            output(task, DEFAULT_SPACES_FIELD, *args)
    logger_settings(get_logger()).info('Only the completed tasks with their subtasks were output.')


def show_all(storage, *args):
    tasks = storage.get_all_tasks()
    for task in tasks:
        if not task.parent_id:
            output(task, DEFAULT_SPACES_FIELD, *args)
            if task.subtasks:
                show_subtasks(task, *args)
    logger_settings(get_logger()).info('The all tasks with their subtasks were output.')


def show_task(storage, task_id, *args):
    tasks = storage.get_all_tasks()
    task = storage.get_task_by_id(tasks, task_id)
    output(task, DEFAULT_SPACES_FIELD, task, *args)


def output(task, spaces=0, name=True, task_id=True, status=True, priority=True, groups=True, created_date=True,
           date_changes=True, deadline=True, user=True, launch_date=True, link=True):
    print()
    if name:
        print(' ' * spaces + 'Name: ' + task.name)
    if task_id:
        print(' ' * spaces + 'ID: ' + task.id)
    if status:
        print(' ' * spaces + 'Status: ' + str(task.status))
    if priority:
        print(' ' * spaces + 'Priority: ' + str(task.priority))
    if groups:
        if task.groups:
            print(' ' * spaces + 'Groups: ' + ', '.join([group for group in task.groups]))
        else:
            print(' ' * spaces + 'Groups: does not belong to any group')
    if created_date:
        print(' ' * spaces + 'Created date: ' + task.creation_date.strftime('%H:%M %Y/%m/%d'))
    if date_changes:
        print(' ' * spaces + 'Date changes: ' + task.change_date.strftime('%H:%M %Y/%m/%d'))
    if user:
        print(' ' * spaces + 'User: ' + str(task.user))
    if deadline:
        task_deadline = task.deadline.strftime('%H:%M %Y/%m/%d') if task.deadline else 'has note'
        print(' ' * spaces + 'Deadline: ' + task_deadline)
    if launch_date:
        task_launch_date = task.launch_date.strftime('%H:%M %Y/%m/%d') if task.launch_date else 'has note'
        print(' ' * spaces + 'Launch date: ' + task_launch_date)
    if link:
        if task.link:
            print(' ' * spaces + 'Link: ' + ', '.join(task.link))
        else:
            print(' ' * spaces + 'Link: has note')


def show_all_groups(storage):
    groups = get_all_groups(storage)
    if groups:
        print('Groups: ' + ', '.join(groups) + '.')
    else:
        print('No task has a group.')
    logger_settings(get_logger()).info('The all groups were output.')


def show_task_groups(storage, id):
    tasks = storage.get_all_tasks()
    task = storage.get_task_by_id(tasks, id)
    if task:
        output(task)
    logger_settings(get_logger()).info('The all groups of the task named ' + str(task.name) + ' were output.')


def show_group_tasks(storage, group):
    group_tasks = get_group_tasks(storage, group)
    if group_tasks:
        print(group + ': ' + ', '.join(group_tasks) + '.')
    else:
        print('No task has this group.')
    logger_settings(get_logger()).info('The all tasks of the group named ' + str(group) + ' were output.')


def show_allgroup_tasks(storage):
    groups = get_all_groups(storage)
    if groups:
        print('Groups:')
        for group in groups:
            group_tasks = get_group_tasks(storage, group)
            print(' ' * GROUP_SPACES_FIELD + group + ': ' + ', '.join(group_tasks) + '.')
    else:
        print('No task has a group.')
    logger_settings(get_logger()).info('Teh all groups with tasks to which these groups belong were output.')


def get_all_groups(storage):
    tasks = storage.filter_tasks(None, None, None, True)
    groups = []
    for task in tasks:
        for group in task.groups:
            if not groups.count(group):
                groups.append(group)
    return groups


def get_group_tasks(storage, group):
    tasks = storage.filter_tasks(None, None, None, True)
    group_tasks = []
    for task in tasks:
        for task_group in task.groups:
            if group == task_group:
                group_tasks.append(task.name)
                break
    return group_tasks
