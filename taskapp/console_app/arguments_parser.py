r"""
The module with generating and parsing command-line arguments
"""

from taskapp.library.logger import get_logger, logger_settings
import argparse


DEFAULT_PRIORITY_FIELD = 'low'


def process_arguments():
    parser = argparse.ArgumentParser()

    parser.add_argument('-st', '--storage_tasks',  help='Path to a file with tasks in the json format')
    parser.add_argument('-sc', '--storage_schedulers', help='Path to a file with schedulers in the json format')
    parser.add_argument('-l', '--logging', action='store_true', help='Enable debugging mode')
    operation_types = parser.add_subparsers(dest='command_line')

    task_parser_commands = operation_types.add_parser('task', help='Task commands')
    task_parser = task_parser_commands.add_subparsers(dest='operations')

    show_task_parser = task_parser.add_parser('show', help='show tasks')
    show_task_parser.add_argument('-td', '--task_id', help='Outputs a task y the entered id')
    show_task_parser.add_argument('-c', '--choice', choices=['active_t', 'active_st', 'completed', 'all'], nargs='?',
                                  default='active_t', help='Choosing which tasks to display by task type')
    show_task_parser.add_argument('-a', '--all_args', action='store_true',
                                  help='Selects whether to output all arguments, or some')
    show_task_parser.add_argument('-n', '--name', action='store_true', help='Name of the task')
    show_task_parser.add_argument('-id', '--id', action='store_true', help='Id of the task')
    show_task_parser.add_argument('-ss', '--status', action='store_true', help='Status of the task')
    show_task_parser.add_argument('-p', '--priority', action='store_true', help='Priority of the task')
    show_task_parser.add_argument('-g', '--group', action='store_true', help='The group to which the task belongs')
    show_task_parser.add_argument('-cd', '--created_date', action='store_true', help='The task creation time')
    show_task_parser.add_argument('-dc', '--date_changes', action='store_true', help='The task last modified')
    show_task_parser.add_argument('-d', '--deadline', action='store_true', help='Deadline of the task')
    show_task_parser.add_argument('-u', '--user', action='store_true', help='User associated with this task')
    show_task_parser.add_argument('-ld', '--launch_date', action='store_true', help='Task start time')
    show_task_parser.add_argument('-l', '--link', action='store_true', help='Task link and link information')

    create_parser = task_parser.add_parser('create', help='create a task')
    create_parser.add_argument('-n', '--name', nargs='*', help='Name of the new task')
    create_parser.add_argument('-id', '--id', help='Id of the PARENT TASK (use this argument when creating a subtask)')
    create_parser.add_argument('-p', '--priority', default=DEFAULT_PRIORITY_FIELD, help='Priority of the new task')
    create_parser.add_argument('-ld', '--launch_date', nargs='*', help='Task start time')
    create_parser.add_argument('-d', '--deadline', nargs='*', help='Deadline for task')
    create_parser.add_argument('-g', '--group', help='The group to which the task belongs')

    set_parser = task_parser.add_parser('set', help='Set a task complete')
    set_parser.add_argument('id', help='ID of the task which is to mark completed')

    delete_parser = task_parser.add_parser('delete', help='Deletes the task')
    delete_parser.add_argument('id', help='ID of the task you want to delete')

    edit_parser = task_parser.add_parser('edit', help='Edit the selected task')
    edit_parser.add_argument('id', help='ID of the task you want to edit')
    edit_parser.add_argument('-n', '--name', nargs='*', help='Name of the task')
    edit_parser.add_argument('-p', '--priority', help='Priority of the task')
    edit_parser.add_argument('-ng', '--new_group', help='Adds a group by deleting the previous ones')
    edit_parser.add_argument('-ag', '--add_group', help='Adds a group, keeping the previous ones')
    edit_parser.add_argument('-d', '--deadline', nargs='*', help='Deadline of the task')

    group_parser_commands = operation_types.add_parser('group', help='Group commands')
    group_parser = group_parser_commands.add_subparsers(dest='group_operations')

    show_group_parser = group_parser.add_parser('show', help='show groups')
    show_group_parser.add_argument('-ag', '--all_groups', action='store_true', help='Displays all existing groups')
    show_group_parser.add_argument('-tg', '--task_groups',
                                   help='Displays all groups of a single task, which is defined by entered ID')
    show_group_parser.add_argument('-agt', '--all_group_tasks', action='store_true',
                                   help='Display all groups with tasks that belong to these groups')
    show_group_parser.add_argument('-gt', '--group_tasks', help='Displays all tasks for the entered group')

    link_parser_commands = operation_types.add_parser('link', help='Work with links in tasks')
    link_parser = link_parser_commands.add_subparsers(dest='link_operations')

    create_link_parser = link_parser.add_parser('create', help='Creates a link between tasks')
    create_link_parser.add_argument('first_task_id', help='Task id to which the binding is (Task A in explanations)')
    create_link_parser.add_argument('second_task_id', help='Task id with which the binding is (Task B in explanations)')
    create_link_parser.add_argument('-lt', '--link_type', choices=['begin-begin', 'begin-end', 'end-begin', 'end-end'],
                                    help='Link "begin-begin" - Task A will not start until Task B begins. '
                                         'Link "begin-end" - Task A will not start until Task B is completed. '
                                         'Link "end-begin" - Task A does not end until task B begins. '
                                         'Link "end-end" - Task A does not end until task B is completed.')

    delete_link_parser = link_parser.add_parser('delete', help='Deletes a link between tasks')
    delete_link_parser.add_argument('task_id', help='Deletes the link in the task')

    scheduler_parser_commands = operation_types.add_parser('scheduler', help='Work with links in tasks')
    scheduler_parser = scheduler_parser_commands.add_subparsers(dest='scheduler_operations')

    create_scheduler_parser = scheduler_parser.add_parser('create', help='create a periodic task')
    create_scheduler_parser.add_argument('-n', '--name', nargs='*', help='Name of the period task')
    create_scheduler_parser.add_argument('-tp', '--type_period', choices=['day', 'month', 'year'], nargs='?',
                                         default='day', help='Task period type')
    create_scheduler_parser.add_argument('period', help='Task period')
    create_scheduler_parser.add_argument('-td', '--type_deadline', choices=['day', 'month', 'year'], nargs='?',
                                         default='day', help='Type of interval between tasks')
    create_scheduler_parser.add_argument('-dp', '--deadline_period', default=None, help='Deadline for each task')
    create_scheduler_parser.add_argument('-p', '--priority', default=DEFAULT_PRIORITY_FIELD,
                                         help='Priority of the new task')
    create_scheduler_parser.add_argument('-sd', '--start_date', nargs='*',
                                         help='The time when the TaskManager starts its work')
    create_scheduler_parser.add_argument('-ed', '--end_date', nargs='*',
                                         help='The time when the TaskManager finishes his work')
    try:
        return parser.parse_args()
    except Exception as exception:
        logger_settings(get_logger()).error(
            'Error when parsing data from console, the name of the exception is ' + str(exception))
