r"""
The module for reading data from config file
"""

import configparser


class Config:
    def __init__(self, path='config.ini'):
        self.config = configparser.ConfigParser()
        self.path = path
        self.config.read(path)

    def get_tasks_path(self):
        path = self.config.get('DEFAULT', 'Tasks', fallback='tasks.json')
        self.config['DEFAULT']['Tasks'] = path
        with open(self.path, 'w') as configfile:
            self.config.write(configfile)
        return path

    def get_schedulers_path(self):
        path = self.config.get('DEFAULT', 'Schedulers', fallback='schedulers.json')
        self.config['DEFAULT']['Schedulers'] = path
        with open(self.path, 'w') as configfile:
            self.config.write(configfile)
        return path

    def get_logging(self):
        logging = self.config.get('DEFAULT', 'Logging', fallback='True')
        self.config['DEFAULT']['Logging'] = logging
        with open(self.path, 'w') as configfile:
            self.config.write(configfile)
        return logging
