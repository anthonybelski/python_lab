r"""
The module which is a console application
"""

from taskapp.console_app import arguments_parser, output_operations
from taskapp.console_app.config import Config
from taskapp.library import logger, operations
from taskapp.library.scheduler import TaskScheduler
from taskapp.library.storage import Storage
from taskapp.library.task import Task


def main():
    arguments = arguments_parser.process_arguments()

    config_file = Config('config.ini')

    if arguments.logging:
        operations.logger_settings(logger.get_logger(), False)

    operations.logger_settings(logger.get_logger(), bool(config_file.get_logging()))

    storage = Storage(config_file.get_tasks_path(), config_file.get_schedulers_path())
    operations.update_tasks(storage)

    if arguments.storage_tasks and arguments.storage_schedulers:
        storage = Storage(arguments.storage_tasks, arguments.storage_schedulers)

    if arguments.command_line == 'task':
        if arguments.operations == 'show':
            if arguments.task_id:
                if arguments.all_args:
                    output_operations.show_task(storage,
                                                arguments.task_id)
                else:
                    output_operations.show_task(storage,
                                                arguments.task_id,
                                                arguments.name,
                                                arguments.id,
                                                arguments.status,
                                                arguments.priority,
                                                arguments.group,
                                                arguments.created_date,
                                                arguments.date_changes,
                                                arguments.deadline,
                                                arguments.user,
                                                arguments.launch_date,
                                                arguments.link)
            elif arguments.choice == 'active_t':
                if arguments.all_args:
                    output_operations.show_tasks(storage)
                else:
                    output_operations.show_tasks(storage,
                                                 arguments.name,
                                                 arguments.id,
                                                 arguments.status,
                                                 arguments.priority,
                                                 arguments.group,
                                                 arguments.created_date,
                                                 arguments.date_changes,
                                                 arguments.deadline,
                                                 arguments.user,
                                                 arguments.launch_date,
                                                 arguments.link)
            elif arguments.choice == 'active_st':
                if arguments.all_args:
                    output_operations.show_with_subtasks(storage)
                else:
                    output_operations.show_with_subtasks(storage,
                                                         arguments.name,
                                                         arguments.id,
                                                         arguments.status,
                                                         arguments.priority,
                                                         arguments.group,
                                                         arguments.created_date,
                                                         arguments.date_changes,
                                                         arguments.deadline,
                                                         arguments.user,
                                                         arguments.launch_date,
                                                         arguments.link)
            elif arguments.choice == 'completed':
                if arguments.all_args:
                    output_operations.show_completed(storage)
                else:
                    output_operations.show_completed(storage,
                                                     arguments.name,
                                                     arguments.id,
                                                     arguments.status,
                                                     arguments.priority,
                                                     arguments.group,
                                                     arguments.created_date,
                                                     arguments.date_changes,
                                                     arguments.deadline,
                                                     arguments.user,
                                                     arguments.launch_date,
                                                     arguments.link)
            elif arguments.choice == 'all':
                if arguments.all_args:
                    output_operations.show_all(storage)
                else:
                    output_operations.show_all(storage,
                                               arguments.name,
                                               arguments.id,
                                               arguments.status,
                                               arguments.priority,
                                               arguments.group,
                                               arguments.created_date,
                                               arguments.date_changes,
                                               arguments.deadline,
                                               arguments.user,
                                               arguments.launch_date,
                                               arguments.link)

        elif arguments.operations == 'create':
            launch_date = " ".join(arguments.launch_date) if arguments.launch_date else arguments.launch_date
            deadline = " ".join(arguments.deadline) if arguments.deadline else arguments.deadline
            operations.add_task(storage,
                                Task(" ".join(arguments.name),
                                     arguments.priority),
                                arguments.id,
                                arguments.group,
                                launch_date,
                                deadline)

        elif arguments.operations == 'set':
            operations.set_task(storage, arguments.id)

        elif arguments.operations == 'delete':
            operations.delete_task(storage, arguments.id)

        elif arguments.operations == 'edit':
            deadline = " ".join(arguments.deadline) if arguments.deadline else arguments.deadline
            operations.edit_task(storage,
                                 arguments.id,
                                 " ".join(arguments.name),
                                 arguments.priority,
                                 arguments.new_group,
                                 arguments.add_group,
                                 deadline)

    elif arguments.command_line == 'group':
        if arguments.group_operations == 'show':
            if arguments.all_groups:
                output_operations.show_all_groups(storage)
            elif arguments.task_groups:
                output_operations.show_task_groups(storage, arguments.task_groups)
            elif arguments.group_tasks:
                output_operations.show_group_tasks(storage, arguments.group_tasks)
            elif arguments.all_group_tasks:
                output_operations.show_allgroup_tasks(storage)

    elif arguments.command_line == 'link':
        if arguments.link_operations == 'create':
            operations.create_link(storage,
                                   arguments.first_task_id,
                                   arguments.second_task_id,
                                   arguments.link_type)

        elif arguments.link_operations == 'delete':
            operations.delete_link(storage,
                                   arguments.task_id)

    elif arguments.command_line == 'scheduler':
        if arguments.scheduler_operations == 'create':
            storage.add_scheduler(TaskScheduler(" ".join(arguments.name),
                                                arguments.priority,
                                                arguments.type_period,
                                                arguments.period,
                                                arguments.type_deadline,
                                                arguments.deadline_period,
                                                arguments.start_date,
                                                arguments.end_date))

    storage.run_schedulers()


if __name__ == '__main__':
    main()
