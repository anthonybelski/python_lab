r"""Console application created for using library functionality

Quick start:

$ taskapp task create -n Go to bed -ld 22:00 -d 23:00
$ taskapp task show -c all -a

> Name: Go to bed
> ID: be38df56
> Status: Not started
> Priority: low
> Groups: does not belong to any group
> Created date: 05:19 2018/08/17
> Date changes: 05:19 2018/08/17
> User: anton
> Deadline: 23:00 2018/08/17
> Launch date: 22:00 2018/08/17
> Link: has note

$ taskapp task create -n Get up -ld 8:00
$ taskapp task show -c all -n -ss -ld

> Name: Go to bed
> Status: Not started
> Launch date: 22:00 2018/08/17

> Name: Get up
> Status: Not started
> Launch date: 08:00 2018/08/17

"""
