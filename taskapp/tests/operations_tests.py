from taskapp.library import operations
from taskapp.library.storage import Storage
from taskapp.library.task import Task
import os
import unittest

FIRST_NAME_FIELD = 'First task'
SECOND_NAME_FIELD = 'Second task'
FIRST_PRIORITY_FIELD = 'low'
SECOND_PRIORITY_FIELD = 'medium'

NEW_NAME_FIELD = 'Modified first task'
NEW_PRIORITY_FIELD = 'high'
NEW_GROUP_FIELD = 'Work'
NEW_LAUNCH_DATE_FIELD = '00:00 2019/10/1'
NEW_DEADLINE_FIELD = '18:30 2019/10/8'

IN_PROCESS_FIELD = 'In the process'
COMPLETED_FIELD = 'Completed'

CONVERTED_DEADLINE_LIST = ['2019', '10', '8', '18', '30']

FIRST_TYPE_LINK = 'begin-begin'
SECOND_TYPE_LINK = 'begin-end'
THIRD_TYPE_LINK = 'end-begin'
FOURTH_TYPE_LINK = 'end-end'

TASKS_FILE_FIELD = 'test_tasks.json'
SCHEDULERS_FILE_FIELD = 'test_schedulers.json'


class TestOperations(unittest.TestCase):
    def setUp(self):
        self.storage = Storage(TASKS_FILE_FIELD, SCHEDULERS_FILE_FIELD)
        self.first_task = Task(FIRST_NAME_FIELD,
                               FIRST_PRIORITY_FIELD)

        self.second_task = Task(SECOND_NAME_FIELD,
                                SECOND_PRIORITY_FIELD)

    def test_add_task(self):
        # add task
        tasks_before = self.storage.get_all_tasks()
        len_before = len(tasks_before)
        operations.add_task(self.storage, self.first_task)
        tasks_after = self.storage.get_all_tasks()
        len_after = len(tasks_after)
        self.assertGreater(len_after, len_before)

        # add task, when it's subtask
        parent_task = self.storage.get_task_by_id(self.storage.get_all_tasks(), self.first_task.id)
        operations.add_task(self.storage, self.second_task, parent_task.id, NEW_GROUP_FIELD, NEW_LAUNCH_DATE_FIELD,
                            NEW_DEADLINE_FIELD)
        subtask = self.storage.get_task_by_id(self.storage.get_all_tasks(), self.second_task.id)
        self.assertEqual(parent_task.id, subtask.parent_id)

        # сheck the transmitted fields for a new task
        self.assertEqual(subtask.groups[0], NEW_GROUP_FIELD)
        list_launch_date = operations.convert_date(NEW_LAUNCH_DATE_FIELD.split())
        self.assertEqual(subtask.launch_date, operations.get_date_from_list(list_launch_date))
        list_deadline = operations.convert_date(NEW_DEADLINE_FIELD.split())
        self.assertEqual(subtask.deadline, operations.get_date_from_list(list_deadline))

    def test_edit_task(self):
        operations.add_task(self.storage, self.first_task)
        task_before = self.storage.get_task_by_id(self.storage.get_all_tasks(), self.first_task.id)
        operations.edit_task(self.storage, task_before.id, NEW_NAME_FIELD, NEW_PRIORITY_FIELD, NEW_GROUP_FIELD, None,
                             NEW_DEADLINE_FIELD)
        task_after = self.storage.get_task_by_id(self.storage.get_all_tasks(), self.first_task.id)
        self.assertEqual(task_after.name, NEW_NAME_FIELD)
        self.assertEqual(task_after.groups[0], NEW_GROUP_FIELD)
        list_deadline = operations.convert_date(NEW_DEADLINE_FIELD.split())
        self.assertEqual(task_after.deadline, operations.get_date_from_list(list_deadline))

    def test_set_task(self):
        operations.add_task(self.storage, self.first_task)
        parent_task_before = self.storage.get_task_by_id(self.storage.get_all_tasks(), self.first_task.id)
        operations.add_task(self.storage, self.second_task, parent_task_before.id)
        subtask_before = self.storage.get_task_by_id(self.storage.get_all_tasks(), self.second_task.id)
        operations.set_task(self.storage, parent_task_before.id)
        parent_task_after = self.storage.get_task_by_id(self.storage.get_all_tasks(), parent_task_before.id)
        subtask_after = self.storage.get_task_by_id(self.storage.get_all_tasks(), subtask_before.id)
        self.assertEqual(parent_task_after.status, COMPLETED_FIELD)
        self.assertEqual(subtask_after.status, COMPLETED_FIELD)

    def test_delete_task(self):
        operations.add_task(self.storage, self.first_task)
        parent_task = self.storage.get_task_by_id(self.storage.get_all_tasks(), self.first_task.id)
        operations.add_task(self.storage, self.second_task, parent_task.id)
        operations.delete_task(self.storage, parent_task.id)
        self.assertFalse(self.storage.get_all_tasks())

    def test_convert_date(self):
        converted_date = operations.convert_date(NEW_DEADLINE_FIELD.split())
        self.assertEqual(converted_date, CONVERTED_DEADLINE_LIST)

    def test_create_link(self):
        operations.add_task(self.storage, self.first_task)
        operations.add_task(self.storage, self.second_task)
        operations.create_link(self.storage, self.first_task.id, self.second_task.id, SECOND_TYPE_LINK)
        linked_task = self.storage.get_task_by_id(self.storage.get_all_tasks(), self.first_task.id)
        self.assertEqual(linked_task.link[0], self.second_task.id)

    def test_delete_link(self):
        operations.add_task(self.storage, self.first_task)
        operations.add_task(self.storage, self.second_task)
        operations.create_link(self.storage, self.first_task.id, self.second_task.id, SECOND_TYPE_LINK)
        linked_task = self.storage.get_task_by_id(self.storage.get_all_tasks(), self.first_task.id)
        operations.delete_link(self.storage, linked_task.id)
        usual_task = self.storage.get_task_by_id(self.storage.get_all_tasks(), self.first_task.id)
        self.assertFalse(usual_task.link)

    def test_update_status(self):
        # the following functions are checked ahead: update_status, create_status, check_link
        operations.add_task(self.storage, self.first_task)
        operations.add_task(self.storage, self.second_task)
        operations.create_link(self.storage, self.first_task.id, self.second_task.id, FOURTH_TYPE_LINK)
        linked_task = self.storage.get_task_by_id(self.storage.get_all_tasks(), self.first_task.id)
        operations.set_task(self.storage, linked_task.id)
        linked_task = self.storage.get_task_by_id(self.storage.get_all_tasks(), self.first_task.id)
        self.assertEqual(linked_task.status, IN_PROCESS_FIELD)
        link_object_task = self.storage.get_task_by_id(self.storage.get_all_tasks(), self.second_task.id)
        operations.set_task(self.storage, link_object_task.id)
        linked_task = self.storage.get_task_by_id(self.storage.get_all_tasks(), self.first_task.id)
        operations.set_task(self.storage, linked_task.id)
        linked_task = self.storage.get_task_by_id(self.storage.get_all_tasks(), self.first_task.id)
        self.assertEqual(linked_task.status, COMPLETED_FIELD)

    def tearDown(self):
        if os.path.exists(TASKS_FILE_FIELD):
            os.remove(TASKS_FILE_FIELD)
        if os.path.exists(SCHEDULERS_FILE_FIELD):
            os.remove(SCHEDULERS_FILE_FIELD)


if __name__ == '__main__':
    unittest.main()
