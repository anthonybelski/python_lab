from taskapp.library import operations
from taskapp.library import json_functions
from taskapp.library.storage import Storage
from taskapp.library.task import Task
import os
import subprocess
import unittest

FIRST_NAME_FIELD = 'First task'
SECOND_NAME_FIELD = 'Second task'
FIRST_PRIORITY_FIELD = 'low'
SECOND_PRIORITY_FIELD = 'medium'

NEW_NAME_FIELD = 'Modified first task'
NEW_PRIORITY_FIELD = 'high'
NEW_GROUP_FIELD = 'Work'
NEW_DEADLINE_FIELD = '18:30 2019/10/8'

NAME_FIELD = 'Go to the theatre'
PRIORITY_FIELD = 'medium'
TYPE_PERIOD_FIELD = 'month'
PERIOD_FIELD = '1'
TYPE_DEADLINE_FIELD = 'day'
DEADLINE_PERIOD_FIELD = '5'
START_DATE_FIELD = '10:00 2017/1/1'
END_DATE_FIELD = '10:00 2019/1/1'

TYPE_LINK = 'end-end'

IN_PROCESS_FIELD = 'In the process'
COMPLETED_FIELD = 'Completed'

SCHEDULERS_FIELD = 'schedulers'

TASKS_FILE_FIELD = 'test_tasks.json'
SCHEDULERS_FILE_FIELD = 'test_schedulers.json'

APP_NAME_FIELD = 'taskapp'


class TestOperations(unittest.TestCase):
    def setUp(self):
        self.storage = Storage(TASKS_FILE_FIELD, SCHEDULERS_FILE_FIELD)
        self.first_task = Task(FIRST_NAME_FIELD,
                               FIRST_PRIORITY_FIELD)
        self.second_task = Task(SECOND_NAME_FIELD,
                                SECOND_PRIORITY_FIELD)

    def test_add_task(self):
        args_list = [APP_NAME_FIELD, '--storage_tasks', TASKS_FILE_FIELD, '--storage_schedulers', SCHEDULERS_FILE_FIELD,
                     'task', 'create', '--name', FIRST_NAME_FIELD, '--priority', FIRST_PRIORITY_FIELD]
        subprocess.call(args_list)
        all_tasks = self.storage.get_all_tasks()
        self.assertEqual(len(all_tasks), 1)

    def test_remove_rask(self):
        operations.add_task(self.storage, self.first_task)
        all_tasks_before = self.storage.get_all_tasks()
        args_list = [APP_NAME_FIELD, '--storage_tasks', TASKS_FILE_FIELD, '--storage_schedulers', SCHEDULERS_FILE_FIELD,
                     'task', 'delete', self.first_task.id]
        subprocess.call(args_list)
        all_tasks_after = self.storage.get_all_tasks()
        self.assertGreater(len(all_tasks_before), len(all_tasks_after))

    def test_set_task(self):
        operations.add_task(self.storage, self.first_task)
        task_before = self.storage.get_task_by_id(self.storage.get_all_tasks(), self.first_task.id)
        self.assertEqual(task_before.status, IN_PROCESS_FIELD)
        args_list = [APP_NAME_FIELD, '--storage_tasks', TASKS_FILE_FIELD, '--storage_schedulers', SCHEDULERS_FILE_FIELD,
                     'task', 'set', self.first_task.id]
        subprocess.call(args_list)
        task_after = self.storage.get_task_by_id(self.storage.get_all_tasks(), self.first_task.id)
        self.assertEqual(task_after.status, COMPLETED_FIELD)

    def test_edit_task(self):
        operations.add_task(self.storage, self.first_task)
        args_list = [APP_NAME_FIELD, '--storage_tasks', TASKS_FILE_FIELD, '--storage_schedulers', SCHEDULERS_FILE_FIELD,
                     'task', 'edit', self.first_task.id, '--name', NEW_NAME_FIELD, '--priority', NEW_PRIORITY_FIELD,
                     '--new_group', NEW_GROUP_FIELD, '--deadline', NEW_DEADLINE_FIELD]
        subprocess.call(args_list)
        task_after = self.storage.get_task_by_id(self.storage.get_all_tasks(), self.first_task.id)
        self.assertEqual(task_after.name, NEW_NAME_FIELD)
        self.assertEqual(task_after.priority, NEW_PRIORITY_FIELD)
        self.assertEqual(task_after.groups[0], NEW_GROUP_FIELD)
        list_deadline = operations.convert_date(NEW_DEADLINE_FIELD.split())
        self.assertEqual(task_after.deadline, operations.get_date_from_list(list_deadline))

    def test_create_link(self):
        operations.add_task(self.storage, self.first_task)
        operations.add_task(self.storage, self.second_task)
        args_list = [APP_NAME_FIELD, '--storage_tasks', TASKS_FILE_FIELD, '--storage_schedulers', SCHEDULERS_FILE_FIELD,
                     'link', 'create', self.first_task.id, self.second_task.id, '--link_type', TYPE_LINK]
        subprocess.call(args_list)
        first_task_after = self.storage.get_task_by_id(self.storage.get_all_tasks(), self.first_task.id)
        self.assertEqual(first_task_after.link[0], self.second_task.id)
        link_type = '-'.join(first_task_after.link[1:])
        self.assertEqual(link_type, TYPE_LINK)

    def test_delete_link(self):
        operations.add_task(self.storage, self.first_task)
        operations.add_task(self.storage, self.second_task)
        operations.create_link(self.storage, self.first_task.id, self.second_task.id, TYPE_LINK)
        first_task_before = self.storage.get_task_by_id(self.storage.get_all_tasks(), self.first_task.id)
        self.assertEqual(first_task_before.link[0], self.second_task.id)
        args_list = [APP_NAME_FIELD, '--storage_tasks', TASKS_FILE_FIELD, '--storage_schedulers', SCHEDULERS_FILE_FIELD,
                     'link', 'delete', self.first_task.id]
        subprocess.call(args_list)
        first_task_after = self.storage.get_task_by_id(self.storage.get_all_tasks(), self.first_task.id)
        self.assertFalse(first_task_after.link)

    def test_create_scheduler(self):
        schedulers = json_functions.read_data(self.storage.schedulers_file)
        scheduler_list_before = []
        if schedulers:
            scheduler_list_before = json_functions.deserialize_schedulers(schedulers[SCHEDULERS_FIELD])
        start_date = START_DATE_FIELD.split()
        end_date = END_DATE_FIELD.split()
        args_list = [APP_NAME_FIELD, '--storage_tasks', TASKS_FILE_FIELD, '--storage_schedulers', SCHEDULERS_FILE_FIELD,
                     'scheduler', 'create', '--name', NAME_FIELD, '--type_period', TYPE_PERIOD_FIELD, PERIOD_FIELD,
                     '--type_deadline', TYPE_DEADLINE_FIELD, '--deadline_period', DEADLINE_PERIOD_FIELD, '--priority',
                     PRIORITY_FIELD, '--start_date', start_date[0], start_date[1], '--end_date', end_date[0],
                     end_date[1]]
        subprocess.call(args_list)
        schedulers = json_functions.read_data(self.storage.schedulers_file)
        scheduler_list_after = []
        if schedulers:
            scheduler_list_after = json_functions.deserialize_schedulers(schedulers[SCHEDULERS_FIELD])
        self.assertGreater(len(scheduler_list_after), len(scheduler_list_before))

    def tearDown(self):
        if os.path.exists(TASKS_FILE_FIELD):
            os.remove(TASKS_FILE_FIELD)
        if os.path.exists(SCHEDULERS_FILE_FIELD):
            os.remove(SCHEDULERS_FILE_FIELD)
        if os.path.exists('config.ini'):
            os.remove('config.ini')


if __name__ == '__main__':
    unittest.main()
