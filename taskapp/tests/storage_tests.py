from taskapp.library import operations
from taskapp.library import json_functions
from taskapp.library.scheduler import TaskScheduler
from taskapp.library.storage import Storage
from taskapp.library.task import Task
import os
import unittest

ALL_TASKS_FIELD = 2

FIRST_NAME_FIELD = 'First task'
SECOND_NAME_FIELD = 'Second task'
FIRST_PRIORITY_FIELD = 'low'
SECOND_PRIORITY_FIELD = 'medium'

NAME_FIELD = 'Go to the theatre'
PRIORITY_FIELD = 'medium'
TYPE_PERIOD_FIELD = 'month'
PERIOD_FIELD = 1
TYPE_DEADLINE_FIELD = 'day'
DEADLINE_PERIOD_FIELD = 5
START_DATE_FIELD = '10:00 2017/1/1'
END_DATE_FIELD = '10:00 2019/1/1'

IN_PROCESS_FIELD = 'In the process'
COMPLETED_FIELD = 'Completed'

SCHEDULERS_FIELD = 'schedulers'

TASKS_FILE_FIELD = 'test_tasks.json'
SCHEDULERS_FILE_FIELD = 'test_schedulers.json'


class TestOperations(unittest.TestCase):
    def setUp(self):
        self.storage = Storage(TASKS_FILE_FIELD, SCHEDULERS_FILE_FIELD)
        self.first_task = Task(FIRST_NAME_FIELD,
                               FIRST_PRIORITY_FIELD)
        self.second_task = Task(SECOND_NAME_FIELD,
                                SECOND_PRIORITY_FIELD)
        self.scheduler = TaskScheduler(NAME_FIELD,
                                       PRIORITY_FIELD,
                                       TYPE_PERIOD_FIELD,
                                       PERIOD_FIELD,
                                       TYPE_DEADLINE_FIELD,
                                       DEADLINE_PERIOD_FIELD,
                                       START_DATE_FIELD.split(),
                                       END_DATE_FIELD.split())

    def test_filter_tasks(self):
        operations.add_task(self.storage, self.first_task)
        operations.add_task(self.storage, self.second_task)
        all_tasks = self.storage.get_all_tasks()
        filtered_tasks = self.storage.filter_tasks(None, None, None, None, None, FIRST_PRIORITY_FIELD)
        self.assertGreater(len(all_tasks), len(filtered_tasks))

    def test_refresh(self):
        operations.add_task(self.storage, self.first_task)
        tasks_before = self.storage.get_all_tasks()
        self.storage.refresh([self.second_task])
        tasks_after = self.storage.get_all_tasks()
        self.assertGreater(len(tasks_after), len(tasks_before))

    def test_get_all_tasks(self):
        operations.add_task(self.storage, self.first_task)
        operations.add_task(self.storage, self.second_task)
        all_tasks = self.storage.get_all_tasks()
        self.assertEqual(ALL_TASKS_FIELD, len(all_tasks))

    def test_get_task_by_id(self):
        operations.add_task(self.storage, self.first_task)
        task = self.storage.get_task_by_id(self.storage.get_all_tasks(), self.first_task.id)
        self.assertEqual(self.first_task.id, task.id)

    def test_upload_tasks(self):
        tasks_before = self.storage.get_all_tasks()
        self.storage.upload_tasks([self.first_task])
        tasks_after = self.storage.get_all_tasks()
        self.assertGreater(len(tasks_after), len(tasks_before))

    def test_add_scheduler(self):
        schedulers = json_functions.read_data(self.storage.schedulers_file)
        scheduler_list_before = []
        if schedulers:
            scheduler_list_before = json_functions.deserialize_schedulers(schedulers[SCHEDULERS_FIELD])
        self.storage.add_scheduler(self.scheduler)
        schedulers = json_functions.read_data(self.storage.schedulers_file)
        scheduler_list_after = []
        if schedulers:
            scheduler_list_after = json_functions.deserialize_schedulers(schedulers[SCHEDULERS_FIELD])
        self.assertGreater(len(scheduler_list_after), len(scheduler_list_before))

    def test_run_schedulers(self):
        tasks_before = self.storage.get_all_tasks()
        self.storage.add_scheduler(self.scheduler)
        self.storage.run_schedulers()
        tasks_after = self.storage.get_all_tasks()
        self.assertGreater(len(tasks_after), len(tasks_before))

    def tearDown(self):
        if os.path.exists(TASKS_FILE_FIELD):
            os.remove(TASKS_FILE_FIELD)
        if os.path.exists(SCHEDULERS_FILE_FIELD):
            os.remove(SCHEDULERS_FILE_FIELD)


if __name__ == '__main__':
    unittest.main()
