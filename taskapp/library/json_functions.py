r"""
The module consists of a function intended for converting data for writing to a json file and reading from a json file
"""

from taskapp.library.scheduler import TaskScheduler
from taskapp.library.task import Task
import datetime
import json
import os


INDENT_SETTING_FIELD = 2
ENSURE_ASCII_FIELD = False
EMPTY_FILE_FIELD = None

IS_DESIRIALIZE_FIELD = True

NAME_FIELD = 'Name'
ID_FIELD = 'ID'
STATUS_FIELD = 'Status'
GROUPS_FIELD = 'Groups'
PRIORITY_FIELD = 'Priority'
PARENT_ID_FIELD = 'Parent ID'
CREATED_DATE_FIELD = 'Created date'
DATE_CHANGES_FIELD = 'Date changes'
DEADLINE_FIELD = 'Deadline'
USER_FIELD = 'User'
LAUNCH_FIELD = 'Launch date'
LINK_FIELD = 'Link'

TASK_NAME_FIELD = 'Task name'
TASK_PRIORITY_FIELD = 'Task priority'
TYPE_PERIOD_FIELD = 'Type period'
PERIOD_FIELD = 'Period'
TYPE_DEADLINE_FIELD = 'Type deadline'
DEADLINE_PERIOD_FIELD = 'Deadline period'
LASTCHECK_DATE_FIELD = 'Last check date'
END_DATE_FIELD = 'End date'


def serialize_tasks(tasks):
    """
    create dictionaries of tasks for subsequent writing to a file
    :param tasks: task list
    :return: list of task dictionaries
    """
    data = []
    for any_task in tasks:
        deadline = list(any_task.deadline.timetuple()[:5]) if any_task.deadline else any_task.deadline
        launch_date = list(any_task.launch_date.timetuple()[:5]) if any_task.launch_date else any_task.launch_date
        task = {NAME_FIELD: any_task.name,
                ID_FIELD: any_task.id,
                STATUS_FIELD: any_task.status,
                PRIORITY_FIELD: any_task.priority,
                PARENT_ID_FIELD: any_task.parent_id,
                GROUPS_FIELD: any_task.groups,
                DEADLINE_FIELD: deadline,
                CREATED_DATE_FIELD: list(any_task.creation_date.timetuple()[:5]),
                DATE_CHANGES_FIELD: list(any_task.change_date.timetuple()[:5]),
                USER_FIELD: any_task.user,
                LAUNCH_FIELD: launch_date,
                LINK_FIELD: any_task.link}
        data.append(task)
    return data


def serialize_schedulers(schedulers):
    """
    create dictionaries of schedulers for subsequent writing to a file
    :param schedulers: scheduler list
    :return: list of scheduler dictionaries
    """
    data = []
    for any_scheduler in schedulers:
        scheduler = {TASK_NAME_FIELD: any_scheduler.task_name,
                     TASK_PRIORITY_FIELD: any_scheduler.task_priority,
                     TYPE_PERIOD_FIELD: any_scheduler.type_period,
                     PERIOD_FIELD: any_scheduler.period,
                     TYPE_DEADLINE_FIELD: any_scheduler.type_deadline,
                     DEADLINE_PERIOD_FIELD: any_scheduler.deadline_period,
                     LASTCHECK_DATE_FIELD: list(any_scheduler.last_check_date.timetuple()[:5]),
                     END_DATE_FIELD: list(any_scheduler.end_date.timetuple()[:5])}
        data.append(scheduler)
    return data


def deserialize_tasks(json_tasks):
    """
    create tasks for further work with them
    :param json_tasks: list of task dictionaries
    :return: task list
    """
    tasks = []
    for any_task in json_tasks:
        deadlin = datetime.datetime(*any_task[DEADLINE_FIELD]) if any_task[DEADLINE_FIELD] else any_task[DEADLINE_FIELD]
        launch_date = datetime.datetime(*any_task[LAUNCH_FIELD]) if any_task[LAUNCH_FIELD] else any_task[LAUNCH_FIELD]
        new_task = Task(any_task[NAME_FIELD],
                        any_task[PRIORITY_FIELD],
                        launch_date,
                        deadlin,
                        any_task[USER_FIELD],
                        any_task[STATUS_FIELD],
                        datetime.datetime(*any_task[CREATED_DATE_FIELD]),
                        datetime.datetime(*any_task[DATE_CHANGES_FIELD]),
                        any_task[ID_FIELD],
                        any_task[PARENT_ID_FIELD],
                        any_task[LINK_FIELD])
        new_task.groups = any_task[GROUPS_FIELD]
        if new_task.parent_id:
            for task in tasks:
                if task.id == new_task.parent_id:
                    task.subtasks.append(new_task)
        tasks.append(new_task)
    return tasks


def deserialize_schedulers(json_schedulers):
    """
    create schedulers for further work with them
    :param json_schedulers: list of scheduler dictionaries
    :return: scheduler list
    """
    schedulers = []
    for any_scheduler in json_schedulers:
        new_scheduler = TaskScheduler(any_scheduler[TASK_NAME_FIELD],
                                      any_scheduler[TASK_PRIORITY_FIELD],
                                      any_scheduler[TYPE_PERIOD_FIELD],
                                      any_scheduler[PERIOD_FIELD],
                                      any_scheduler[TYPE_DEADLINE_FIELD],
                                      any_scheduler[DEADLINE_PERIOD_FIELD],
                                      datetime.datetime(*any_scheduler[LASTCHECK_DATE_FIELD]),
                                      datetime.datetime(*any_scheduler[END_DATE_FIELD]),
                                      IS_DESIRIALIZE_FIELD)
        schedulers.append(new_scheduler)
    return schedulers


def read_data(file_name):
    try:
        with open(os.path.join(os.getcwd(), file_name)) as file:
            data = json.load(file)
    except IOError:
        data = EMPTY_FILE_FIELD
    return data


def write_data(data, file_name):
    with open(os.path.join(os.getcwd(), file_name), 'w') as file:
        json.dump(data, file, indent=INDENT_SETTING_FIELD, ensure_ascii=ENSURE_ASCII_FIELD)
