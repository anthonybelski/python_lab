r""" Library for library for storage and work with tasks.

Modules of package:
json_functions.py - the module for converting data for writing to a json file and reading from a json file
logger.py - the module with logger settings.
operations.py - the module with operations on tasks
scheduler.py - the module contains the entity that creates tasks by rules
storage.py - the module for collecting, filtering etc models of lib
task.py - the module consists of such an entity as a task

Quick start:

>>> from taskapp.library import operations, storage, task

>>> storage = storage.Storage()
>>> first_task = task.Task(name="new_task", priority="high")
>>> operations.add_task(storage, first_task)
>>> second_task = task.Task(name="another_new_task", priority="low")
>>> operations.add_task(storage, second_task)

# create a link between tasks
>>> operations.create_link(storage, first_task.id, second_task.id)

# delete task
>>> operations.delete_task(storage, first_task.id)

# mark task as complete
>>> operations.set_task(storage, second_task.id)

"""