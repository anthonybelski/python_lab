r"""
The module consists of such an entity as a task
"""

import datetime
import getpass
import uuid


NONE_FIELD = None

HIGH_FIELD = 'high'
MEDIUM_FIELD = 'medium'
LOW_FIELD = 'low'

NOT_STARTED_FIELD = 'Not started'
IN_PROCESS_FIELD = 'In the process'
COMPLETED_FIELD = 'Completed'
FAILED_FIELD = 'Failed'


class Task:
    def __init__(self, name, priority='low', launch_date=None, deadline=None, user=None, status=None,
                 creation_date=None, change_date=None, existing_id=None, parent_id=None, link=None):
        self.name = name

        if priority in [HIGH_FIELD, MEDIUM_FIELD, LOW_FIELD]:
            self.priority = priority
        else:
            self.priority = NONE_FIELD

        self.status = status
        self.user = user if user else getpass.getuser()
        self.link = link if link else []

        self.deadline = deadline
        if deadline:
            if creation_date:
                self.deadline = deadline
            else:
                time_args = [int(item) for item in deadline]
                self.deadline = datetime.datetime(*time_args)

            if datetime.datetime.now().strftime('%Y%m%d%H%M') >= self.deadline.strftime('%Y%m%d%H%M'):
                self.status = FAILED_FIELD

        self.launch_date = launch_date
        if launch_date:
            if creation_date:
                self.launch_date = launch_date
            else:
                time_args = [int(item) for item in launch_date]
                self.launch_date = datetime.datetime(*time_args)

        if self.status != COMPLETED_FIELD and self.status != FAILED_FIELD:
            if len(self.link) < 1:
                if self.launch_date:
                    if self.deadline:
                        if self.deadline.strftime('%Y%m%d%H%M') <= self.launch_date.strftime('%Y%m%d%H%M'):
                            self.status = FAILED_FIELD
                    if datetime.datetime.now().strftime('%Y%m%d%H%M') >= self.launch_date.strftime('%Y%m%d%H%M'):
                        self.status = IN_PROCESS_FIELD
                    else:
                        self.status = NOT_STARTED_FIELD
                else:
                    self.status = IN_PROCESS_FIELD

        self.id = existing_id if existing_id else uuid.uuid4().hex[:8]
        self.parent_id = parent_id

        self.creation_date = creation_date if creation_date else datetime.datetime.now()
        self.change_date = change_date if change_date else self.creation_date

        self.groups = []
        self.subtasks = []
