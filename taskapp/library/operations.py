r"""
The module with basic operations on tasks
"""

from taskapp.library.logger import get_logger, logger_settings
import datetime
import time


NOT_STARTED_FIELD = 'Not started'
IN_PROCESS_FIELD = 'In the process'
COMPLETED_FIELD = 'Completed'
FAILED_FIELD = 'Failed'

BEGIN_TYPE_FIELD = 'begin'
END_TYPE_FIELD = 'end'

MAX_DATE_LENGTH = 2


def add_task(storage, task, parent_id=None, group=None, launch_date=None, deadline=None):
    """
    check whether the created task is a subtask and call a function from the storage to save the task
    :param storage: storage object
    :param task: object created task
    :param parent_id: the id of the parent task. It exists if the task is a subtask
    :param group: the name of the group to which the task belongs
    :param launch_date: task start date
    :param deadline: deadline date
    """
    task.launch_date = get_date_from_list(convert_date(launch_date.split())) if launch_date else launch_date
    task.deadline = get_date_from_list(convert_date(deadline.split())) if deadline else deadline
    if group:
        task.groups.append(group)
    if parent_id:
        task.parent_id = parent_id
    storage.refresh([task])
    logger_settings(get_logger()).info('Was added task named ' + str(task.name) + '.')


def edit_task(storage, task_id, new_name=None, new_priority=None, new_group=None, add_group=None, new_deadline=None):
    """
    change task fields and call a function from the storage to save changes
    :param storage: storage object
    :param task_id: the id of the task whose fields will be changed
    :param new_name: new task name
    :param new_priority: new task priority
    :param new_group: new task group
    :param add_group: one more task group
    :param new_deadline: new task deadline
    """
    tasks = storage.filter_tasks(IN_PROCESS_FIELD)
    for any_task in tasks:
        if any_task.id == task_id:
            if new_name:
                any_task.name = new_name
                any_task.change_date = datetime.datetime.now()
            if new_priority:
                any_task.priority = new_priority
                any_task.change_date = datetime.datetime.now()
            if new_group:
                any_task.groups = [new_group]
            elif add_group:
                if not any_task.groups.count(add_group):
                    any_task.groups.append(add_group)
            if new_deadline:
                new_deadline = get_date_from_list(convert_date(new_deadline.split())) if new_deadline else new_deadline
                any_task.deadline = new_deadline
                any_task.change_date = datetime.datetime.now()

            logger_settings(get_logger()).info('The task named ' + str(new_name) + ' was modified.')
            storage.upload_tasks(tasks)
            break
    else:
        raise KeyError('The entered id does not exist')


def set_task(storage, task_id):
    """
    transfer the task to the "completed" state and call a function from the storage to save changes
    :param storage: storage object
    :param task_id: the id of the task whose fields will be changed
    """
    tasks = storage.get_all_tasks()
    for task in tasks:
        if task.id == task_id:
            if len(task.link) > 1:
                # check whether the task can be transferred to the "completed" state if the following types of links are
                # established: end-start, end-end
                link_object_id = task.link[0]
                link_object = storage.get_task_by_id(tasks, link_object_id)
                type_link_task = task.link[1]
                type_link_object = task.link[2]
                if type_link_task == END_TYPE_FIELD:
                    if type_link_object == BEGIN_TYPE_FIELD:
                        if link_object.status == IN_PROCESS_FIELD or link_object.status == COMPLETED_FIELD:
                            complete_task(task)
                    elif type_link_object == END_TYPE_FIELD:
                        if link_object.status == COMPLETED_FIELD:
                            complete_task(task)

            else:
                complete_task(task)
            storage.upload_tasks(tasks)
            break
    else:
        raise KeyError('The entered id does not exist')


def complete_task(task):
    if task.status == IN_PROCESS_FIELD:
        task.status = COMPLETED_FIELD
        task.change_date = datetime.datetime.now()
        set_subtasks(task)
        logger_settings(get_logger()).info('The task named ' + str(task.name) + ' was completed with her subtasks.')


def set_subtasks(task):
    for subtask in task.subtasks:
        if subtask.status == IN_PROCESS_FIELD:
            subtask.status = COMPLETED_FIELD
            subtask.change_date = datetime.datetime.now()
            if subtask.subtasks:
                set_subtasks(subtask)


def delete_task(storage, task_id):
    """
    delete the task and call a function from the storage to save changes
    :param storage: storage object
    :param task_id: the id of the task which you need to delete
    """
    tasks = storage.filter_tasks(IN_PROCESS_FIELD)
    for any_task in tasks:
        if any_task.id == task_id:
            if any_task.subtasks:
                delete_subtasks(storage, any_task.subtasks, tasks)
            tasks.remove(any_task)
            logger_settings(get_logger()).info('The task named ' + str(any_task.name) +
                                               ' was removed with her subtasks.')
            storage.upload_tasks(tasks)
            break
    else:
        raise KeyError('The entered id does not exist')


def delete_subtasks(storage, subtasks, tasks):
    for subtask in subtasks:
        if subtask.subtasks:
            delete_subtasks(storage, subtask.subtasks, tasks)
        else:
            tasks.remove(subtask)


def convert_date(date):
    """
    convert the date from the user input format to the json file format
    :param date: date in the format by which it is entered by the user
    :return: date in json file format
    """
    if date:
        if len(date) > MAX_DATE_LENGTH:
            raise ValueError('The date was entered incorrectly')

        test_time = date[0]
        try:
            time.strptime(test_time, '%H:%M')
        except ValueError:
            raise ValueError('The date was entered incorrectly')

        enter_date = date[:]
        day_time = enter_date.pop(0).split(':')
        date_now = datetime.datetime.now()
        if date_now.strftime('%H%M') > ''.join(day_time):
            date = [str(date_now.year), str(date_now.month), str(date_now.day + 1)]
        else:
            date = [date_now.year, date_now.month, date_now.day]
        date.extend(day_time)
        if enter_date:
            test_year = enter_date[0]
            try:
                time.strptime(test_year, '%Y/%m/%d')
            except ValueError:
                raise ValueError('The date was entered incorrectly')

            year_time = enter_date.pop(0).split('/')
            date = year_time
            date.extend(day_time)

    return date


def create_link(storage, first_task_id, second_task_id, link_type=None):
    """
    create a link between tasks (one-way link) and call a function from the storage to save changes
    :param storage: storage object
    :param first_task_id: the id of the task that will depend on another task
    :param second_task_id: the id of the task that will be associated with another task
    :param link_type: type of link, one of the following: 'begin-begin', 'begin-end', 'end-begin', 'end-end'
    """

    tasks = storage.get_all_tasks()
    first_task = storage.get_task_by_id(tasks, first_task_id)
    second_task = storage.get_task_by_id(tasks, second_task_id)
    if first_task and second_task:
        if not second_task.link:
            first_task.link = []
            first_task.link.append(second_task_id)
            if link_type:
                link_type_list = link_type.split('-')
                first_task.link.extend(link_type_list)
    else:
        raise KeyError('The entered id does not exist')
    storage.upload_tasks(tasks)
    logger_settings(get_logger()).info('The link has been created from the task named ' + str(first_task.name) + '.')


def delete_link(storage, task_id):
    """
    delete a link between tasks (one-way link) and call a function from the storage to save changes
    :param storage: storage object
    :param task_id: the id of the task in which the link will be deleted
    """
    tasks = storage.get_all_tasks()
    task = storage.get_task_by_id(tasks, task_id)
    if task:
        task.link = []
    else:
        raise KeyError('The entered id does not exist')
    storage.upload_tasks(tasks)
    logger_settings(get_logger()).info('The link has been removed from the task named ' + str(task.name) + '.')


def update_tasks(storage):
    """
    update task fields and call a function from the storage to save changes
    :param storage: storage object
    """
    tasks = storage.get_all_tasks()
    update_status(storage, tasks)
    storage.upload_tasks(tasks)
    logger_settings(get_logger()).info('Tasks have been updated.')


def update_status(storage, tasks):
    """
    check if it is worth updating the status in accordance with the links
    :param storage: storage object
    :param tasks: task list
    """
    for task in tasks:
        if len(task.link) > 1:
            create_status(task)
            logger_settings(get_logger()).info('The status of the task named ' + str(task.name) + ' was changed.')
    for task in tasks:
        if len(task.link) > 1:
            if task.link[1] == BEGIN_TYPE_FIELD:
                if task.status == IN_PROCESS_FIELD:
                    check_link(storage, tasks, task)
                    logger_settings(get_logger()).info(
                        'The status of the task named ' + str(task.name) + ' was changed.')


def create_status(task):
    """
    updates the status according to the fields associated with the date
    :param task: task object
    """
    if task.status != COMPLETED_FIELD and task.status != FAILED_FIELD:
        if task.launch_date:
            if task.deadline:
                if task.deadline.strftime('%Y%m%d%H%M') <= task.launch_date.strftime('%Y%m%d%H%M'):
                    task.status = FAILED_FIELD
            elif datetime.datetime.now().strftime('%Y%m%d%H%M') >= task.launch_date.strftime('%Y%m%d%H%M'):
                task.status = IN_PROCESS_FIELD
            else:
                task.status = NOT_STARTED_FIELD
        else:
            task.status = IN_PROCESS_FIELD


def check_link(storage, tasks, task):
    """
    check if it's worth changing the status according to the links
    :param storage: storage object
    :param tasks: task list
    :param task: task object
    """
    link_object_id = task.link[0]
    link_object = storage.get_task_by_id(tasks, link_object_id)
    type_link_object = task.link[2]
    if task.status != FAILED_FIELD or task.status != COMPLETED_FIELD:
        if type_link_object == BEGIN_TYPE_FIELD:
            if link_object.status == IN_PROCESS_FIELD or link_object.status == COMPLETED_FIELD:
                task.status = IN_PROCESS_FIELD
            else:
                task.status = NOT_STARTED_FIELD
        elif type_link_object == END_TYPE_FIELD:
            if link_object.status == COMPLETED_FIELD:
                task.status = IN_PROCESS_FIELD
            else:
                task.status = NOT_STARTED_FIELD


def get_date_from_list(date_list):
    time_args = [int(item) for item in date_list]
    return datetime.datetime(*time_args)
