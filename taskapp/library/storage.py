r"""
The module contains Storage, which filters tasks by fields and saves data in json files
"""

from taskapp.library import json_functions


DEFAULT_SPACES_FIELD = 0

TASKS_FIELD = 'tasks'
SCHEDULERS_FIELD = 'schedulers'

TASKS_FILE_FIELD = 'tasks.json'
SCHEDULERS_FILE_FIELD = 'schedulers.json'


class Storage:
    def __init__(self, tasks_file=None, schedulers_file=None):
        self.tasks_file = tasks_file if tasks_file else TASKS_FILE_FIELD
        self.schedulers_file = schedulers_file if schedulers_file else SCHEDULERS_FILE_FIELD

    def filter_tasks(self, status=None, is_task=None, all_fields=None, is_groups_exist=None, name=None, priority=None,
                     deadline=None, user=None, creation_date=None, change_date=None, group=None):
        json_tasks = json_functions.read_data(self.tasks_file)
        tasks = []
        if json_tasks:
            tasks = json_functions.deserialize_tasks(json_tasks[TASKS_FIELD])
        filtered_tasks = []

        if name:
            filtered_tasks = [x for x in tasks if x.name == name]
        if priority:
            filtered_field = [x for x in tasks if x.priority == priority]
            filtered_tasks = list(set(filtered_tasks) & set(filtered_field)) if filtered_tasks else filtered_field
        if status:
            filtered_field = [x for x in tasks if x.status == status]
            filtered_tasks = list(set(filtered_tasks) & set(filtered_field)) if filtered_tasks else filtered_field
        if is_task:
            filtered_field = [x for x in tasks if x.parent_id is None]
            filtered_tasks = list(set(filtered_tasks) & set(filtered_field)) if filtered_tasks else filtered_field
        if deadline:
            filtered_field = [x for x in tasks if x.deadline == deadline]
            filtered_tasks = list(set(filtered_tasks) & set(filtered_field)) if filtered_tasks else filtered_field
        if user:
            filtered_field = [x for x in tasks if x.user == user]
            filtered_tasks = list(set(filtered_tasks) & set(filtered_field)) if filtered_tasks else filtered_field
        if creation_date:
            filtered_field = [x for x in tasks if x.creation_date == creation_date]
            filtered_tasks = list(set(filtered_tasks) & set(filtered_field)) if filtered_tasks else filtered_field
        if change_date:
            filtered_field = [x for x in tasks if x.change_date == change_date]
            filtered_tasks = list(set(filtered_tasks) & set(filtered_field)) if filtered_tasks else filtered_field
        if group:
            filtered_field = [x for x in tasks if group in x.groups]
            filtered_tasks = list(set(filtered_tasks) & set(filtered_field)) if filtered_tasks else filtered_field
        if is_groups_exist:
            filtered_field = [x for x in tasks if x.groups]
            filtered_tasks = list(set(filtered_tasks) & set(filtered_field)) if filtered_tasks else filtered_field
        if all_fields:
            filtered_tasks = tasks

        return filtered_tasks

    def refresh(self, new_tasks):
        """
        add a tasks with a file with the rest of the tasks and call a function to save changes
        :param new_tasks: new task objects
        """
        tasks = json_functions.read_data(self.tasks_file)
        new_json_tasks = json_functions.serialize_tasks(new_tasks)
        if tasks:
            json_tasks = tasks[TASKS_FIELD]
            json_tasks.extend(new_json_tasks)
        else:
            json_tasks = new_json_tasks
        tasks = {TASKS_FIELD: json_tasks}
        json_functions.write_data(tasks, self.tasks_file)

    def get_all_tasks(self):
        return self.filter_tasks(None, None, True)

    def get_task_by_id(self, tasks, id):
        for task in tasks:
            if task.id == id:
                return task
        return False

    def upload_tasks(self, tasks):
        tasks = {TASKS_FIELD: json_functions.serialize_tasks(tasks)}
        json_functions.write_data(tasks, self.tasks_file)

    def add_scheduler(self, scheduler):
        """
        add a scheduler with the rest of the schedulers and call a function to save changes
        :param scheduler: scheduler object
        """
        schedulers = json_functions.read_data(self.schedulers_file)
        scheduler_list = []
        if schedulers:
            scheduler_list = json_functions.deserialize_schedulers(schedulers[SCHEDULERS_FIELD])

        scheduler_list.append(scheduler)

        schedulers = {SCHEDULERS_FIELD: json_functions.serialize_schedulers(scheduler_list)}
        json_functions.write_data(schedulers, self.schedulers_file)

    def run_schedulers(self):
        """
        start schedulers and delete the completed and call a function to save changes
        """
        schedulers = json_functions.read_data(self.schedulers_file)
        scheduler_list = []
        if schedulers:
            scheduler_list = json_functions.deserialize_schedulers(schedulers[SCHEDULERS_FIELD])

        for scheduler in scheduler_list:
            if scheduler.try_start():
                self.refresh(scheduler.start())
            if scheduler.try_complete():
                scheduler_list.remove(scheduler)

        schedulers = {SCHEDULERS_FIELD: json_functions.serialize_schedulers(scheduler_list)}
        json_functions.write_data(schedulers, self.schedulers_file)
