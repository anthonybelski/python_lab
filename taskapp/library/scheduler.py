r"""
The module contains the entity that creates tasks by rules
"""

from dateutil.relativedelta import relativedelta
from taskapp.library import operations
from taskapp.library.task import Task
import datetime


DAYS_FIELD = 'day'
MONTHS_FIELD = 'month'
YEARS_FIELD = 'year'


class TaskScheduler:
    def __init__(self, task_name, task_priority, type_period, period, type_deadline, deadline_period=None,
                 start_date=None, end_date=None, is_deserialize=False):
        self.task_name = task_name
        self.task_priority = task_priority
        self.type_period = type_period
        self.period = int(period)
        self.type_deadline = type_deadline
        self.deadline_period = int(deadline_period)
        # (last_check_date) -> when creating the same as date from which the creation of periodic tasks begins,
        # only this date is constantly changing so as not to include tasks that have already been created:
        if is_deserialize:
            self.last_check_date = start_date
            self.end_date = end_date
        else:
            self.last_check_date = self.create_date(start_date) if start_date else start_date
            self.end_date = self.create_date(end_date) if end_date else end_date

        self.current_time = datetime.datetime.now()

    def create_date(self, date):
        date = operations.convert_date(date)
        time_args = [int(item) for item in date]
        return datetime.datetime(*time_args)

    def try_start(self):
        if self.current_time.strftime('%Y%m%d%H%M') >= self.last_check_date.strftime('%Y%m%d%H%M'):
            return True
        else:
            return False

    def try_complete(self):
        if self.current_time.strftime('%Y%m%d%H%M') >= self.end_date.strftime('%Y%m%d%H%M'):
            return True
        else:
            return False

    def add_time(self, date, time_type, amount_of_time):
        if time_type == DAYS_FIELD:
            return date + relativedelta(days=+amount_of_time)
        elif time_type == MONTHS_FIELD:
            return date + relativedelta(months=+amount_of_time)
        elif time_type == YEARS_FIELD:
            return date + relativedelta(years=+amount_of_time)

    def create_repeat_tasks(self, date):
        """
        Create tasks for the period of time from the last application of the application to the current moment
        :param date: datetime object
        :return: list of created tasks
        """
        tasks = []
        while date.strftime('%Y%m%d%H%M') > self.last_check_date.strftime('%Y%m%d%H%M'):
            deadline = self.add_time(self.last_check_date, self.type_deadline, self.deadline_period)
            tasks.append(Task(self.task_name, self.task_priority, None, deadline.timetuple()[:5]))
            self.last_check_date = self.add_time(self.last_check_date, self.type_period, self.period)
        return tasks

    def start(self):
        if self.try_complete():
            return self.create_repeat_tasks(self.end_date)
        else:
            return self.create_repeat_tasks(self.current_time)
