r"""
The module with logger settings.
"""

import logging

NAME_APP = 'Taskapp'


def get_logger():
    return logging.getLogger(NAME_APP)


def logger_settings(logger, enabled=None):
    """
    set the settings for the logger
    :param logger: logger object
    :param enabled: enable / disable logging
    :return: customized logger object
    """
    logger.setLevel(logging.DEBUG)

    if not logger.handlers:
        logger.disabled = enabled

        formatter = logging.Formatter('%(levelname)s : %(name)s : %(message)s')

        file_handler = logging.StreamHandler()
        file_handler.setFormatter(formatter)

        logger.addHandler(file_handler)

    return logger