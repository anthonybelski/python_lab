from setuptools import setup, find_packages

setup(
    name='task_app',
    version='1.0',
    packages=find_packages(),
    entry_points={
        'console_scripts':
            ['taskapp = taskapp.console_app.app:main']
    },
    url='',
    license='',
    author='Anton',
    author_email='',
    description=''
)
