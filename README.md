# Taskapp

## Installation

First, go to the directory where you want to install the application.
Next, download this repository using this command:

    git clone https://anthonybelski@bitbucket.org/anthonybelski/python_lab.git

Then install the application using this command:

    python python_lab/setup.py install

### Quick start

After installation, you can enter a command 'taskapp -h' in the terminal to familiarize yourself with the application's capabilities.

### Examples:

Console application:



    $ taskapp task create -n Go to bed -ld 22:00 -d 23:00
    $ taskapp task show -c all -a

    > Name: Go to bed
    > ID: be38df56
    > Status: Not started
    > Priority: low
    > Groups: does not belong to any group
    > Created date: 05:19 2018/08/17
    > Date changes: 05:19 2018/08/17
    > User: anton
    > Deadline: 23:00 2018/08/17
    > Launch date: 22:00 2018/08/17
    > Link: has note

    $ taskapp task create -n Get up -ld 8:00
    $ taskapp task show -c all -n -ss -ld

    > Name: Go to bed
    > Status: Not started
    > Launch date: 22:00 2018/08/17

    > Name: Get up
    > Status: Not started
    > Launch date: 08:00 2018/08/17


Library:



    >>> from taskapp.library import operations, storage, task

    >>> storage = storage.Storage()
    >>> first_task = task.Task(name="new_task", priority="high")
    >>> operations.add_task(storage, first_task)
    >>> second_task = task.Task(name="another_new_task", priority="low")
    >>> operations.add_task(storage, second_task)

    # create a link between tasks
    >>> operations.create_link(storage, first_task.id, second_task.id)

    # delete task
    >>> operations.delete_task(storage, first_task.id)

    # mark task as complete
    >>> operations.set_task(storage, second_task.id)